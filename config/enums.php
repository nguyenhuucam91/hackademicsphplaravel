<?php
return [

    'role' => [
        'super_admin' => 0,
        'admin' => 1,
        'staff' => 2,
        'customer' => 3
    ],

    'email' => [
        'email_verification_status' => [
            'not_confirmed' => 0,
            'confirmed' => 1
        ],

        'email_recovery_duration' => 30
    ]
];