<?php

use Illuminate\Database\Seeder;

class CatBreedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Models\Animal\Cats::class, 300)->create();
       factory(\App\Models\UserAccount\UserAccount::class)->create();
    }
}
