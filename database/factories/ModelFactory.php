<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\UserAccount::class, function (Faker\Generator $faker) {

    return [
        'name' => 'admin',
        'email' => 'aten040791@gmail.com',
        'password' => 'secret',
        'remember_token' => str_random(10),
        'role' => config('enums.role.super_admin')
    ];
});


$factory->define(\App\Models\Breed::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
    ];
});


$factory->define(\App\Models\Cats::class, function (Faker\Generator $faker) use ($factory) {
    $startDate = \Carbon\Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addHour();
    return [
        'name' => $faker->name,
        'gender' => $faker->boolean(),
        'phone_number' => '01112223333',
        'dob' => $faker->date('Y-m-d'),
        'breed_id' => factory(\App\Models\Breed::class)->create()->id,
        'created_at' => $startDate,
        'updated_at' => $endDate
    ];
});
