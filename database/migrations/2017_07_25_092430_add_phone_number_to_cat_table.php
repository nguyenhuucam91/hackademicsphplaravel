<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneNumberToCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cats', 'phone_number')) {
            Schema::table('cats', function(Blueprint $table){
                $table -> string('phone_number', 13)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cats', 'phone_number')) {
            Schema::table('cats', function($table)
            {
                $table->dropColumn('phone_number');
            });
        }
    }
}
