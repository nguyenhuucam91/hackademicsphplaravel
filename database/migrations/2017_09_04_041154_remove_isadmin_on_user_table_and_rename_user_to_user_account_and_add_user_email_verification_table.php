<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIsadminOnUserTableAndRenameUserToUserAccountAndAddUserEmailVerificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasTable('user_accounts')) {
                Schema::rename('users', 'user_accounts');
            }
        }

        if (Schema::hasTable('user_accounts')) {
            if (Schema::hasColumn('user_accounts', 'is_admin')) {
                Schema::table('user_accounts', function (Blueprint $table) {
                    $table->renameColumn('is_admin', 'role');
                });
            }
        }

        Schema::dropIfExists('emails');
        Schema::dropIfExists('users');

        if (!Schema::hasTable('user_email_verifications')) {
            Schema::create('user_email_verifications', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('user_account_id');
                //when user confirmed email, set verification_token to NULL
                $table->string('verification_token', 100)->nullable();
                // expired time.
                $table->dateTime('verification_token_expired')->nullable();
                $table->tinyInteger('email_verification_status')->default(config('enums.email_verification_status.not_confirmed'));
                $table->foreign('user_account_id')->references('user_accounts')->on('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_email_verifications');

    }
}
