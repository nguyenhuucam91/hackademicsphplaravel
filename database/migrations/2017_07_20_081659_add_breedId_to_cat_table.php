<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBreedIdToCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cats', 'breed_id')) {
            Schema::table('cats', function (Blueprint $table) {
                $table->unsignedInteger('breed_id');
                $table->foreign('breed_id')->references('id')->on('breeds');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cats', 'breed_id')) {
            Schema::table('cats', function (Blueprint $table) {
                $table->removeColumn('breed_id');
            });
        }
    }
}
