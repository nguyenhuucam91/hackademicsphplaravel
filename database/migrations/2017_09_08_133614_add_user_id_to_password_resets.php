<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToPasswordResets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('password_resets')) {
            Schema::rename('password_resets', 'user_password_resets');
        }

        if (!Schema::hasColumn('user_password_resets', 'date_expired')) {
            Schema::table('user_password_resets', function (Blueprint $table) {
                $table->dateTime('expiration_time');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('user_password_resets', 'date_expired')) {
            Schema::table('user_password_resets', function (Blueprint $table) {
                $table->removeColumn('expiration_time');
            });
        }

        if (Schema::hasTable('user_password_resets')) {
            Schema::rename('user_password_resets', 'password_resets');
        }
    }
}
