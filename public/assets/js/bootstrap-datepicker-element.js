$(function(){
    var datepickerElement = $('.datepicker');
    var hasClassDatepickerInPage = datepickerElement.length;
    var jsResourceLocation = '/assets/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js';
    var cssResourceLocation = '/assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css';

    if (hasClassDatepickerInPage > 0) {
        $("head").append("<link rel='stylesheet'  href='" + cssResourceLocation + "'/>");
        $.getScript(jsResourceLocation, function () {
            datepickerElement.datepicker({
                format: "dd/mm/yyyy",
                todayBtn: "linked"
            });
        });
    }
});


