$(function () {
    var arrayInitializer = [];
    $("input[name='check-selected']").change(function () {

        var checkSelected = $("input[name='check-selected']:checked").length;

        if (checkSelected > 0) {
            $('.operations__delete--hide').removeClass('hide');
        }
        else {
            $('.operations__delete--hide').addClass('hide');
        }
        appendSelectedValueToHiddenField($(this).val(), $("input[name='ids']"));
    });


    function appendSelectedValueToHiddenField(value, targetElement) {
        var index = arrayInitializer.indexOf(value);
        if (index > -1) {
            arrayInitializer.splice(index);
        }

        else {
            arrayInitializer.push(value);
        }

        var valueToAppend = arrayInitializer.join();
        targetElement.val(valueToAppend);
    }
});

