<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InterfaceBindingProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $bindings = [
            "App\\Repositories\\Contracts\\CatRepositoryInterface" => "App\\Repositories\\Eloquents\\CatRepository",
            "App\\Repositories\\Contracts\\BreedRepositoryInterface" => "App\\Repositories\\Eloquents\\BreedRepository",
            "App\\Repositories\\Contracts\\UserAccountRepositoryInterface" => "App\\Repositories\\Eloquents\\UserAccountRepository",
            "App\\Repositories\\Contracts\\UserEmailVerificationRepositoryInterface" => "App\\Repositories\\Eloquents\\UserEmailVerificationRepository",
            "App\\Repositories\\Contracts\\UserForgotPasswordRepositoryInterface" => "App\\Repositories\\Eloquents\\UserForgotPasswordRepository",

            /**
             * Mail binding
             */
            "App\\Notifications\\Mail\\MailInterface" => "App\\Notifications\\Mail\\TextEmail\\UserForgotPasswordTextEmail",
        ];

        foreach ($bindings as $key => $value) {
            $this->app->bind($key, $value);
        }
    }
}
