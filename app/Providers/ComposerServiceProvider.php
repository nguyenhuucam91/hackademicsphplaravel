<?php

namespace App\Providers;

use App\Http\ViewComposer\CatFormComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $viewComposers = [
                'partial.layouts.cat' => CatFormComposer::class
        ];

        foreach($viewComposers as $key => $value){
            view()->composer($key, $value);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
