<?php

namespace App\Providers;

use App\Events\SendEmailEvent;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $listeners = [
        'App\\Events\\UserAccount\\UserRegisteredEvent' => [
            'App\\Listeners\\UserAccount\\SendEmailListener'
        ],
    ];

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
