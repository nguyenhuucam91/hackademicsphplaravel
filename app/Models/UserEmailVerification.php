<?php

namespace App\Models\UserEmailVerification;

use App\Models\Eloquent;

class UserEmailVerification extends Eloquent
{
    protected $fillable = ['verification_token', 'verification_token_expired', 'email'];

    public $timestamps = false;

    public $dates = ['verification_token_expired'];
}
