<?php

namespace App\Models;

class UserForgotPassword extends Eloquent
{
    protected $fillable = ['token', 'email', 'created_at', 'expiration_time'];

    protected $dates = ['created_at', 'expiration_time'];

    protected $table = 'user_password_resets';

    public $timestamps = false;

    public function userAccount() {
        return $this->belongsTo(UserAccount::class, 'email', 'email');
    }
}
