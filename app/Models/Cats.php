<?php

namespace App\Models;

use App\Filters\CatFilters;
use App\Presenters\EloquentPresenter\CatPresenter;

//cannot use abstract here, because class Cats already extends
class Cats extends Eloquent
{
    protected $fillable = ['name', 'dob', 'breed_id', 'phone_number'];

    /**
     * Application setting
     * @var string
     */

    protected $presenter = CatPresenter::class;
    protected $filter = CatFilters::class;

    /**
     * Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function breed()
    {
        return $this->belongsTo(Breed::class, 'breed_id', 'id');
    }

    /**
     * Mutator**
     * @param $value
     */
    public function setDobAttribute($value)
    {
        $formattedDate = $this->storeFormattedDate($value);
        $this->attributes['dob'] = $formattedDate;
    }

}
