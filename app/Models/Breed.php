<?php

namespace App\Models;

use App\Presenters\EloquentPresenter\BreedPresenter;

class Breed extends Eloquent
{
    protected $fillable = ['name'];
    protected $presenter = BreedPresenter::class;

    public function cats(){
        return $this->hasMany(Cats::class, 'breed_id','id');
    }

    #region App\Eloquent Members


    #endregion
}
