<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/18/2017
 * Time: 6:19 PM
 */

namespace App\Models;

use App\Presenters\PresentableTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class Eloquent extends Model
{
    use PresentableTrait;
    use ModelScope;

    public function storeFormattedDate($value)
    {
        if (strpos($value, '/') > 0) {
            $date = Carbon::createFromFormat('d/m/Y', $value);
            return $date->format('Y-m-d');
        }
        return $value;
    }

}