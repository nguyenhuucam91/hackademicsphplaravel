<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/28/2017
 * Time: 10:59 AM
 */

namespace App\Models;

trait ModelScope
{

    /**
     * Serve for list of scopes to be implemented
     */


    /**
     * Scope a query to get filter
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function scopeFilter($query)
    {
        $filter = app()->make($this->filter);
        return $filter->apply($query);
    }

}