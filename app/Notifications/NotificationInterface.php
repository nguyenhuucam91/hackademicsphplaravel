<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/17/2017
 * Time: 9:43 PM
 */

namespace App\Notifications;


interface NotificationInterface
{
    public function via($notifiable);
    public function toMail($notifiable);
    public function toArray($notifiable);
}