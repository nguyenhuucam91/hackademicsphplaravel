<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/17/2017
 * Time: 6:49 PM
 */

namespace App\Notifications\Mail;


use App\Notifications\NotificationInterface;

interface MailInterface extends NotificationInterface
{

}