<?php

namespace App\Notifications\Mail\TextEmail;

use App\Models\UserAccount;
use App\Models\UserForgotPassword;
use App\Notifications\Mail\MailInterface;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserForgotPasswordTextEmail extends Notification implements MailInterface
{
    use Queueable;

    private $user;
    /**
     * @var UserForgotPassword
     */
    private $userForgotPassword;

    /**
     * @var UserForgotPasswordRepositoryInterface
     */

    public function __construct(UserAccount $user, UserForgotPassword $userForgotPassword)
    {
        $this->user = $user;
        $this->userForgotPassword = $userForgotPassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject("Reset password request")->view('mail.text.forgot-password-mail', [
            'user' => $this->user,
            'userForgotPassword' => $this->userForgotPassword
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
