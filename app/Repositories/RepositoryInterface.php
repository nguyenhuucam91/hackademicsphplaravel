<?php
/**
 * Created by PhpStorm.
 * UserAccount: nguye
 * Date: 7/29/2017
 * Time: 10:41 PM
 */

namespace App\Repositories;

interface RepositoryInterface {
    public function all($columns = ['*']);
    public function create(array $data);
    public function update($id, array $data);
    public function destroy($ids);
    public function findByPk($id);
    public function findWhere($column, $operator = null, $value = null, $boolean = 'and');
    public function findFirst($column, $operator = null, $value = null, $boolean = 'and');
}