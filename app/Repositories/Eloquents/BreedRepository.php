<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/5/2017
 * Time: 10:58 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Breed;
use App\Repositories\Contracts\BreedRepositoryInterface;

class BreedRepository extends CommonEloquentRepository implements BreedRepositoryInterface
{
    protected $model = Breed::class;
}