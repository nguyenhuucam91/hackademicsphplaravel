<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 9/4/2017
 * Time: 6:35 PM
 */

namespace App\Repositories\Eloquents;


use App\Models\UserForgotPassword;
use App\Repositories\Contracts\UserEmailVerificationRepositoryInterface;

class UserEmailVerificationRepository extends CommonEloquentRepository implements UserEmailVerificationRepositoryInterface
{
    protected $model = UserForgotPassword::class;
}