<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/30/2017
 * Time: 9:44 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\UserAccount;
use App\Repositories\Contracts\UserAccountRepositoryInterface;

class UserAccountRepository extends CommonEloquentRepository implements UserAccountRepositoryInterface
{
    protected $model = UserAccount::class;

    public function findOrCreate(array $attributes, array $values = [])
    {
        $user = $this->model->firstOrCreate($attributes, $values);
        return $user;
    }
}