<?php
/**
 * Created by PhpStorm.
 * UserAccount: nguye
 * Date: 8/1/2017
 * Time: 4:14 PM
 */

namespace App\Repositories\Eloquents;

use App\Models\Cats;
use App\Repositories\Contracts\CatRepositoryInterface;

class CatRepository extends CommonEloquentRepository implements CatRepositoryInterface
{
    protected $model = Cats::class;

}