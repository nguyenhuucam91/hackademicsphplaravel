<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/18/2017
 * Time: 11:09 PM
 */

namespace App\Repositories\Eloquents;


use App\Models\UserForgotPassword;
use App\Repositories\Contracts\UserForgotPasswordRepositoryInterface;

class UserForgotPasswordRepository extends CommonEloquentRepository implements UserForgotPasswordRepositoryInterface
{
    protected $model = UserForgotPassword::class;

}