<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/16/2017
 * Time: 11:40 PM
 */

namespace App\Repositories\Eloquents;


use App\Repositories\Repository;

class CommonEloquentRepository extends Repository
{
    protected $model;

    /*
    |--------------------------------------------------------------------------
    | Find section
    |--------------------------------------------------------------------------
    |
    | Supported section for finding such as findFirst(), findWhere() and all()
    |
    */
    public function all($columns = ['*'])
    {
        return $this->model->all();
    }

    public function findWhere($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->model->where($column, $operator, $value, $boolean);
    }

    public function findFirst($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->model->where($column, $operator, $value, $boolean)->first();
    }

    public function findByPk($id)
    {
        return $this->model->findOrFail($id);
    }

    public function allWithPaginate($columns = ['*'], $perPage = 15)
    {
        $itemPerPageInConfig = config('view.per_page');
        $itemPerPage = isset($itemPerPageInConfig) ? $itemPerPageInConfig : $perPage;
        //columns is either array or string.
        $columnsSelected = $this->makeTypeFactory($columns)->generate();
        return $this->model->select($columnsSelected)->paginate($itemPerPage);
    }

    /*
   |--------------------------------------------------------------------------
   | Create section
   |--------------------------------------------------------------------------
   |
   | Supported section for object creation
   |
   */

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /*
   |--------------------------------------------------------------------------
   | Update section
   |--------------------------------------------------------------------------
   |
   | Supported section for updating section
   |
   */

    public function update($id, array $data)
    {
        $model = $this->findByPk($id);
        return $model->update($data);
    }

    /*
   |--------------------------------------------------------------------------
   | Delete section
   |--------------------------------------------------------------------------
   |
   | Supported section for deleting.
   |
   */

    public function destroy($ids)
    {
        return $this->model->destroy($ids);
    }

    /*
     |--------------------------------------------------------------------------
     | Miscellaneous section
     |--------------------------------------------------------------------------
     |
     | Supported section for other operations
     |
     */

    public function filter($request)
    {
        return $this->model->filter($request)->paginate(config('view.per_page'));
    }

}