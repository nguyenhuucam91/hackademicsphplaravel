<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 9/4/2017
 * Time: 6:34 PM
 */

namespace App\Repositories\Contracts;


use App\Repositories\RepositoryInterface;

interface UserEmailVerificationRepositoryInterface extends RepositoryInterface
{

}