<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/30/2017
 * Time: 10:37 PM
 */

namespace App\Repositories\Contracts;


use App\Repositories\RepositoryInterface;

interface UserAccountRepositoryInterface extends RepositoryInterface
{
    public function findOrCreate(array $attributes, array $values = []);

}