<?php

namespace App\Repositories\Contracts;

use App\Filters\CatFilters;
use App\Repositories\RepositoryInterface;

interface CatRepositoryInterface extends RepositoryInterface {
    public function filter($request);
    public function allWithPaginate($columns = ['*'], $perPage = 15);
}