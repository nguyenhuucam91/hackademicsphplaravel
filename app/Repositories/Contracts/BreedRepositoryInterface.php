<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/5/2017
 * Time: 10:57 PM
 */

namespace App\Repositories\Contracts;


use App\Repositories\RepositoryInterface;

interface BreedRepositoryInterface extends RepositoryInterface
{
    public function allWithPaginate($columns = ['*'], $perPage = 15);
}