<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/18/2017
 * Time: 11:24 PM
 */

namespace App\Repositories\Contracts;


use App\Repositories\RepositoryInterface;

interface UserForgotPasswordRepositoryInterface extends RepositoryInterface
{

}