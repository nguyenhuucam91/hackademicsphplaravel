<?php

namespace App\Repositories;

use App\Exceptions\NoFilterClassException;
use App\Exceptions\RepositoryException;
use App\Filters\QueryFilter;
use App\Presenters\EloquentPresenter\AbstractPresenter;
use Illuminate\Database\Eloquent\Model;

class Repository
{
    protected $factory;
    protected $model;

    public function __construct()
    {
        $this->makeModel();
    }

    public function makeModel()
    {
        if (empty($this->model) || !isset($this->model)) {
            throw new RepositoryException("Set attribute protected model to your " . get_class($this) . " repository");
        }

        $model = app()->make($this->model);

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function getModel() {
        return $this->model;
    }

}