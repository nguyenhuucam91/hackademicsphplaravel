<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/17/2017
 * Time: 10:38 AM
 */

namespace App\Filters;


class CatFilters extends CommonFilters
{
    public function breed($id = '')
    {
        if (strcmp($id, '') == 0) {
            return $this->builder->get();
        }
        $this->builder->where('breed_id', $id);

    }

    public function gender($gender = '') {
        if (strcmp($gender, '') == 0) {
            return $this->builder->get();
        }
        return $this->builder->where('gender', $gender);
    }

    public function keyword($keyword = ''){
        if (strcmp($keyword, '') == 0) {
            return $this->builder->get();
        }
        return $this->builder->where('name', 'like' , '%'.$keyword.'%');
    }
}
