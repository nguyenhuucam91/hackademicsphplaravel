<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/17/2017
 * Time: 10:57 AM
 */

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    protected $request;

    protected $builder;

    /**
     * QueryFilter constructor.
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->request->all() as $name => $value) {
            //['foo' => 'bar', 'length' => '2', 'abc'=>null] => using scope Laravel to handle this, for example: scopeFoo(), scopeLength();
            if (method_exists($this, $name)) {   //$name refers to breeds(), length() ....
                if (strcmp($value, '') != 0) {
                    $this->$name($value);
                } else {
                    $this->$name();
                }
            }           
        }
        return $this->builder;
    }
}