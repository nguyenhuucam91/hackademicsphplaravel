<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/22/2017
 * Time: 4:22 PM
 */

namespace App\Filters;

class CommonFilters extends QueryFilter
{
    protected $items;

    /**
     * Pagination
     * @param int $start
     * @return mixed
     */
    public function start($start = '')
    {
        $this->items = count($this->builder->get());
        if (strcmp($start, ' ') == 0) {
            $start = 0;
        }
        return $this->builder->limit(config('view.per_page'))->offset($start);
    }

    public function items()
    {
        return $this->items;
    }

    //sorting goes here.

    public function sort($fieldWithDirection = '')
    {
        if ($fieldWithDirection !== "") {
            $sortFragments = explode('_', $fieldWithDirection);
            $direction = $sortFragments[1];
            $column = $sortFragments[0];
            return $this->builder->orderBy($column, $direction);
        }
    }
}