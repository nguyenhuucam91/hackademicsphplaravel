<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/25/2017
 * Time: 10:07 AM
 */

namespace App\Presenters\EloquentPresenter;


use Carbon\Carbon;

class CommonPresenters extends AbstractPresenter
{
    public function created_at()
    {
        if ($this->entity->created_at == null || strlen($this->entity->created_at) == 0) {
            return '';
        }
        $dobRawData = Carbon::createFromFormat('Y-m-d H:m:s', $this->entity->created_at);
        return $dobRawData->format('d/m/Y H:m:s');
    }

    public function updated_at()
    {
        if ($this->entity->updated_at == null || strlen($this->entity->updated_at) == 0) {
            return '';
        }
        $dobRawData = Carbon::createFromFormat('Y-m-d H:m:s', $this->entity->updated_at);
        return $dobRawData->format('d/m/Y H:m:s');
    }

    public function startItem($currentPage = null)
    {
        $page = 0;
        if (isset($currentPage) || $currentPage != null) {
            $page = $currentPage - 1;
        }

        return config('view.per_page') * $page + 1;
    }
}