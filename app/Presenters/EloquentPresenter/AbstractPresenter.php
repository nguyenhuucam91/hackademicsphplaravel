<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/23/2017
 * Time: 3:53 PM
 */

namespace App\Presenters\EloquentPresenter;


use App\Presenters\PresentableTrait;

abstract class AbstractPresenter
{
    protected $entity;
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    //auto resolve name property: ie -> cat->presenter()->name => cat->presenter->name()
    public function __get($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        }
        return $this->entity->$name;
    }

}