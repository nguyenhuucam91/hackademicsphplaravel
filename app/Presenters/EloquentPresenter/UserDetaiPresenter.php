<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/29/2017
 * Time: 9:37 AM
 */

namespace App\Presenters\EloquentPresenter;

use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Support\Facades\Auth;

class UserDetaiPresenter extends CommonPresenters
{
    public function isAdmin(){
        return $this->entity->is_admin;
    }
}