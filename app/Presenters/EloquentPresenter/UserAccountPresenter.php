<?php
/**
 * Created by PhpStorm.
 * User: camnh
 * Date: 9/8/2017
 * Time: 6:06 PM
 */

namespace App\Presenters\EloquentPresenter;

use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Support\Facades\Auth;

class UserAccountPresenter extends CommonPresenters
{
    public function gravatar()
    {
        if (Gravatar::exists(Auth::user()->email)) {
            return Gravatar::get(Auth::user()->email);
        }
        return Gravatar::fallback("http://www.gravatar.com/avatar/3b3be63a4c2a439b013787725dfce802?d=identicon")->get(Auth::user()->email);
    }

    public function name()
    {
        return strtolower($this->entity->name);
    }

    public function email()
    {
        if (strpos($this->entity->email, '@') > 0) {
            $emailFragments = explode('@', $this->entity->email);
            return $emailFragments[0];
        }
        return $this->entity->email;
    }
}