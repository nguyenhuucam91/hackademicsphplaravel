<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/23/2017
 * Time: 4:09 PM
 */

namespace App\Presenters\EloquentPresenter;


use Carbon\Carbon;

class CatPresenter extends CommonPresenters
{
    public function name()
    {        
        return strtolower($this->entity->name);
    }

    public function dob()
    {
        if (strlen($this->entity->dob) == 0) {
            return '';
        }
        $dobRawData = Carbon ::createFromFormat('Y-m-d', $this->entity->dob);
        return $dobRawData->format('d/m/Y');
    }

}
