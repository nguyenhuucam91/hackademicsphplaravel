<?php
/**
 * Created by PhpStorm.
 * Users: camnh
 * Date: 8/24/2017
 * Time: 9:17 AM
 */

namespace App\Presenters;

use App\Exceptions\AttributesNotFoundException;

trait PresentableTrait
{
    protected $presenterInstance;

    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter) || !property_exists($this, 'presenter')) {
            throw new AttributesNotFoundException('Please set the $protected presenter property to your '. get_class($this));
        }

        if ($this->presenterInstance == null) {
            $this->presenterInstance = new $this->presenter($this);
        }

        return $this->presenterInstance;
    }

}