<?php
/**
 * Created by PhpStorm.
 * UserAccount: camnh
 * Date: 8/28/2017
 * Time: 11:46 AM
 */

function sort_by($column, $linkContent, $routeName)
{
    $direction = getDirection();
    return link_to_route($routeName, $linkContent,
        array_merge(request()->all(), ['sort' => "$column" . '_' . "$direction"]));
}

function getDirection()
{
    $direction = "asc";
    if (request()->has('sort')) {
        $sortWithDirection = request()->get('sort');
        if ($sortWithDirection !== null) {
            $sortFragments = explode('_', $sortWithDirection);
            $direction = $sortFragments[1] === "asc" ? "desc" : "asc";
        }

    }
    return $direction;
}

function redirect_to($routeName, $linkTitle = null, $params = [], $attrs = [])
{
    return link_to_route($routeName, $linkTitle, $params, $attrs);
}