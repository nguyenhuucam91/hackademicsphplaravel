<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminResetPasswordRequest;
use App\Repositories\Eloquents\UserAccountRepository;
use App\Repositories\Eloquents\UserForgotPasswordRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/';
    private $accountRepository;
    private $forgotPasswordRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserAccountRepository $accountRepository,
        UserForgotPasswordRepository $forgotPasswordRepository
    ) {
        $this->middleware('guest');
        $this->accountRepository = $accountRepository;
        $this->forgotPasswordRepository = $forgotPasswordRepository;
    }

    /**
     * GET
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetPasswordForm(Request $request, $token = null)
    {
        $hasError = false;
        if (!empty($token) || $token != null) {
            $forgotPasswordInstance = $this->forgotPasswordRepository->findFirst('token', $token);
            if ($forgotPasswordInstance == null) {
                $hasError = true;
            }

            if (!Carbon::now()->between($forgotPasswordInstance->created_at, $forgotPasswordInstance->expiration_time)) {
                $hasError = true;
            }
        } else {
            $hasError = true;
        }

        if ($hasError) {
            $request->session()->flash('warning', "Link thay đổi mật khẩu không đúng hoặc đã hết hiệu lực.
                    Để thay đổi vui lòng click vào " . link_to_route('password.request',
                    'đây') . " để lấy link khởi tạo mật khẩu.");
            return view('partial.notifications.notifications');
        } else {
            return view('auth.passwords.recover', compact('token'));
        }
    }

    public function resetPassword(AdminResetPasswordRequest $request, $token)
    {
        $password = $request->get('password');
        $forgotPasswordInstance = $this->forgotPasswordRepository->findFirst('token', $token);
        if ($forgotPasswordInstance != null) {
            $emailRecover = $forgotPasswordInstance->email;
            $accountInstance = $this->accountRepository->findFirst('email', '=', $emailRecover);
            if (count($accountInstance) > 0) {
                $recoverPasswordData = [
                    'password' => $password,
                    'email' => $emailRecover
                ];

                $this->accountRepository->update($accountInstance->id, $recoverPasswordData);
                $request->session()->flash('success', "Cập nhật tài khoản thành công. Ấn vào ".redirect_to('login', "đây")." để đăng nhập");
                return view('partial.notifications.notifications');
            }
        }
        $request->session()->flash('warning',
            "Link thay đổi mật khẩu không đúng hoặc đã hết hiệu lực.
                    Để thay đổi vui lòng click vào " . link_to_route('password.request',
                'đây') . " để lấy link khởi tạo mật khẩu.");
    }

}
