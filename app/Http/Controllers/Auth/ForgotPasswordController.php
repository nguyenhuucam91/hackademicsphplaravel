<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\MailNotSentException;
use App\Http\Controllers\Controller;
use App\Notifications\Mail\MailInterface;
use App\Repositories\Contracts\UserAccountRepositoryInterface;
use App\Repositories\Eloquents\UserForgotPasswordRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;
    /**
     * @var MailInterface
     */
    private $mail;
    /**
     * @var UserAccountRepositoryInterface
     */
    private $userAccountRepository;
    /**
     * @var UserEmailVerificationRepositoryInterface
     */
    private $forgotPasswordRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserAccountRepositoryInterface $userAccountRepository,
        UserForgotPasswordRepository $forgotPasswordRepository,
        MailInterface $mail
    ) {
        $this->middleware('guest');
        $this->mail = $mail;
        $this->userAccountRepository = $userAccountRepository;
        $this->forgotPasswordRepository = $forgotPasswordRepository;
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.reset');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $receiverEmail = $request->get('email');
        $userInstance = $this->userAccountRepository->findFirst('email', '=', $receiverEmail);
        if (count($userInstance) > 0) {
            try {
                $emailRecoverData = [
                    'email' => $receiverEmail,
                    'token' => str_random(30),
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'expiration_time' => Carbon::now()->addMinute(config('enums.email.email_recovery_duration'))->format('Y-m-d H:i:s'),
                ];
                //create new record with token to reset password.
                $forgotPasswordInstance = $this->forgotPasswordRepository->create($emailRecoverData);
                //sending email notification
                Notification::send($userInstance, new $this->mail($userInstance, $forgotPasswordInstance));
                $request->session()->flash('success', 'Check your email inbox');
                return view('partial.notifications.notifications');
            }
            catch (MailNotSentException $e) {

            }
        } else {
            $request->session()->flash('error', 'Your email is not exist');
        }
        return redirect()->route('password.request');
    }
}
