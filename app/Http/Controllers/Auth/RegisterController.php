<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\UserAccount\UserAccountCreateRequest;
use App\Models\UserAccount\UserAccount;
use App\Repositories\Contracts\UserAccountRepositoryInterface;
use App\Repositories\Contracts\UserEmailVerificationRepositoryInterface;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '';

    protected $iUserEmailVerificationRepository;

    protected $iUserAccountRepository;

    public function __construct(
        UserAccountRepositoryInterface $iUserAccountRepository,
        UserEmailVerificationRepositoryInterface $iUserEmailVerificationRepository
    ) {

        $this->middleware('guest');
        $this->iUserAccountRepository = $iUserAccountRepository;
        $this->iUserEmailVerificationRepository = $iUserEmailVerificationRepository;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function register(UserAccountCreateRequest $request)
    {
        DB::transaction(function () use ($request) {
            $request->request->add(['role' => config('enums.role.customer')]);
            $userAccount = $this->iUserAccountRepository->create($request->all());
            $this->iUserEmailVerificationRepository->create([
                'user_account_id' => $userAccount->id,
                'verification_token' => str_random(100),
                'verification_token_expired' => Carbon::parse(Carbon::now())->addMinute(config('auth.timeExpired.emailExpiredTime')),
                'email_verification_status' => config('enums.email.email_verification_status.not_confirmed')
            ]);
            $this->guard()->login($userAccount);

            return $this->registered($request, $userAccount)
                ?: redirect($this->redirectPath());
        });

    }

}
