<?php

namespace App\Http\Controllers;

use App\Filters\CatFilters;
use App\Http\Requests\CatCreateRequest;
use App\Models\Animal\Cats;
use App\Repositories\Contracts\BreedRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\Contracts\CatRepositoryInterface;

class CatsController extends Controller
{
    protected $catRepositoryInterface;
    protected $breedRepositoryInterface;

    public function __construct(       
        CatRepositoryInterface $catRepositoryInterface,
        BreedRepositoryInterface $breedRepositoryInterface
    ) {
        $this->catRepositoryInterface = $catRepositoryInterface;
        $this->breedRepositoryInterface = $breedRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breeds = $this->breedRepositoryInterface->all()->pluck('name', 'id');
        $cats = $this->catRepositoryInterface->allWithPaginate();
        return view('admin.cats.index', [
            'cats' => $cats,
            'breeds' => $breeds,
            'request' => request()->all(),
        ]);
    }

    /**
     * Show the layouts for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breeds = $this->breedRepositoryInterface->all()->pluck('name', 'id');
        return view('admin.cats.create', [
            'cat' => new Cats(),
            'breeds' => $breeds
        ]);
    }

    /**
     * Filter cats based on parameters
     * @param CatFilters $catsFilter
     * @return mixed
     */

    public function filter(Request $request, CatFilters $catFilters)
    {        
        $catsFiltered = $this->catRepositoryInterface->filter($catFilters);
        $breeds = $this->breedRepositoryInterface->all()->pluck('name', 'id');
        return view('admin.cats.index', [
            'cats' => $catsFiltered,
            'breeds' => $breeds,
            'request' => $request->all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CatCreateRequest $request)
    {
        $this->catRepositoryInterface->create($request->all());
        return redirect()->route('admin.cats.index')->with('success', 'created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Cats $cat)
    {
        return view('admin.cats.show', ['cat' => $cat]);
    }

    /**
     * Show the layouts for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Cats $cat)
    {
        $breeds = $this->breedRepositoryInterface->all()->pluck('name', 'id');
        return view('admin.cats.edit', [
            'cat' => $cat,
            'breeds' => $breeds
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cats $cat)
    {
        $this->catRepositoryInterface->update($cat->id, $request->all());
        return redirect()->route('admin.cats.show', ['cat' => $cat])->with('success', 'edited');
    }

    /**
     * @param Cats $cat
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Cats $cat)
    {
        $this->catRepositoryInterface->destroy($cat->id);
        return redirect()->route('admin.cats.index')->with('success', 'deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $catIds = explode(",", request()->ids);
        $this->catRepositoryInterface->destroy($catIds);
        return redirect()->route('admin.cats.index')->with('success', 'deleted');
    }

}
