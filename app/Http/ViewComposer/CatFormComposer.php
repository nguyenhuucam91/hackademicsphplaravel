<?php
namespace App\Http\ViewComposer;
use App\Breed;
use Illuminate\Contracts\View\View;

class CatFormComposer
{
    protected $breeds;

    public function __construct(Breed $breeds)
    {
        $this->breeds = $breeds;
    }

    public function compose(View $view)
    {
        $view->with('breeds', $this->breeds->pluck('name', 'id'));
    }
}