@extends('layouts.login')

@section('title')
    Login
@endsection


@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin Panel</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="{{route('login')}}" method="post">
                        {{csrf_field()}}
                        <fieldset>

                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                <div class="invalid-feedback">
                                    @if($errors->has('email'))
                                        {{$errors->first('email')}}
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password"
                                       value="">
                                <div class="invalid-feedback">
                                    @if($errors->has('password'))
                                        {{$errors->first('password')}}
                                    @endif
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>

                            <!-- Login via socialite -->
                            <hr/>

                            <p><a href="{{ route('password.request') }}">Forgot password?</a></p>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
