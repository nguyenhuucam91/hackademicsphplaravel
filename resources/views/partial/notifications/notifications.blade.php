@extends('layouts.login')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Messages from system</div>
                <div class="panel-body">
                    @include('partial.flash.flash')
                </div>
            </div>
        </div>
    </div>
@endsection