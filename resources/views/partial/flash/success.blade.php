@if (request()->session()->has('success') && strlen(request()->session()->get('success')) > 0 )

	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Success!</strong> {!! request()->session()->get('success') !!}
	</div>

@endif
