@if (request()->session()->has('warning') && strlen(request()->session()->get('warning')) > 0 )

    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <strong>Warning: </strong> {!!  request()->session()->get('warning') !!}
    </div>

@endif
