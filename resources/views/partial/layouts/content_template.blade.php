@extends('layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    @yield('heading')                  
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">

                @yield('operations')

                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('panel_heading')
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        @yield('main_content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- customizing js -->
    <script src="{{ asset('assets/js/bootstrap-datepicker-element.js') }}"></script>
    <script src="{{ asset('assets/js/delete-selected-button.js') }}"></script>
@endsection