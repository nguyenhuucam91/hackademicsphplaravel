<!-- Bootstrap Core CSS -->
<link href="{{asset('assets/components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet"/>

<!-- MetisMenu CSS -->
<link href="{{asset('assets/components/metisMenu/dist/metisMenu.css')}}" rel="stylesheet"/>

<!-- Custom CSS -->
<link href={{asset('css/sb-admin-2.min.css')}} rel="stylesheet"/>

<!-- Morris Charts CSS -->
<link href="{{asset('assets/components/morrisjs/morris.css')}}" rel="stylesheet"/>

<!-- Custom Fonts -->
<link href="{{asset('assets/components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<link rel="stylesheet" href="{{ asset('css/bootstrap-social.css') }}" />