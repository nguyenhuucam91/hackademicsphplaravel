<!DOCTYPE html>
<html lang="en">

<head>

    @include('layouts.meta')

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    @include('layouts.css')

    @yield('css')

</head>
<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        @include('layouts.nav.nav_top')
        @include('layouts.nav.nav-left')
    </nav>

    @yield('content')
</div>
.
@include('layouts.scripts')

@yield('scripts')

</body>
</html>
