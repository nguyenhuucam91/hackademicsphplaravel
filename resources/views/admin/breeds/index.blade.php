@extends('partial.layouts.content_template')

@section('title')
    List breed
@endsection

@section('heading')
    Manage breeds
@endsection

@section('operations')
    <p class="operations">
        <a href="{{route('admin.breeds.create')}}" class="btn btn-primary">
            Create
        </a>

        <a class="operations__delete--hide hide btn btn-danger"
           onclick="document.getElementById('delete-selected-form').submit()">Delete selected
        </a>
    </p>

    <form action="{{ route('admin.breeds.destroy') }}" id="delete-selected-form" method="POST">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <input type="hidden" name="ids"/>
    </form>
@endsection

@section('panel_heading')
    List breed
@endsection

@section('main_content')

    @include('partial.flash.success')

    <br/>

    @if(count($breeds) > 0)
        <table class="table table-striped table-bordered">
            <thead class="thead-default">
            <tr>
                <th><input type="checkbox"/></th>
                <th>#</th>
                <th>Name</th>
                <th>Updated at</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>

            @foreach($breeds as $key => $breed)
                <tr>
                    <td><input type="checkbox" name="check-selected" value="{{ $breed->id }}"/></td>
                    <td>{{ $breed->present()->startItem(request()->get('page')) + $key }}</td>
                    <td>{{ $breed->name }}</td>
                    <td>{{ $breed->present()->updated_at }}</td>
                    <td>
                        <a href="{{route('admin.cats.edit', ['cat' => $breed])}}">
                            <button class="btn btn-info">Edit</button>
                        </a>
                        <button class="btn btn-danger"
                                onclick="document.getElementById('delete-cat-{{$breed->id}}').submit()">
                            Delete
                        </button>

                        <form id="delete-cat-{{$breed->id}}" style="display: none" method="POST"
                              action="{{route('admin.cats.delete', ['cat' => $breed])}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    @else
        @include('partial.table.empty')
    @endif
    <!-- /.table-responsive -->
    {{ $breeds->appends(request()->all())->links() }}
    <!-- Pagination -->
@stop
