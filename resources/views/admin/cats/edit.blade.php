@extends('partial.layouts.content_template')

@section('title')
    Edit cat
@endsection

@section('css')
    <link rel="stylesheet"
          href="{{ asset('assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}"/>
@endsection

@section('heading')
    Edit cat #{{ $cat->id }}
@endsection

@section('panel_heading')
    Enter cat information
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.notifications.errors')

            {!! Form::open(['url' => route('admin.cats.update', ['cat' => $cat]), 'method' => 'PATCH']) !!}
            @include('admin.cats._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection