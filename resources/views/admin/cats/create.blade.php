@extends('partial.layouts.content_template')

@section('heading')
    Create new cat
@endsection

@section('panel_heading')
    Enter cat information
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.notifications.errors')

            {!! Form::open(['url' => route('admin.cats.store'), 'method' => 'POST', 'role' => 'form']) !!}
            @include('admin.cats._form')
            {!! Form::close() !!}
        </div>
    </div>
@stop
