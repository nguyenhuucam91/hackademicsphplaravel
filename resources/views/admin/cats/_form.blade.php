<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', $cat->name, ['class' => 'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('date_of_birth', 'Date of Birth') !!}
    {!! Form::text('dob', $cat->present()->dob, ['class' => 'form-control datepicker']) !!}

</div>
<div class="form-group">
    {!! Form::label('breed_id', 'Breed') !!}
    {!! Form::select('breed_id', $breeds, $cat->breed_id, ['class' => 'form-control']) !!}

</div>

<div class="form-group">
    {!! Form::label('phone_number', 'Phone number (0XXXYYYZZZZ)') !!}
    {!! Form::text('phone_number', $cat->phone_number, ['class' => 'form-control']) !!}

</div>

<div class="form-group">
    {!! Form::label('Gender') !!}

    @if ($cat->gender == 1 || strlen($cat->gender) == 0)
        {!! Form::radio('gender', '1', true) !!} Male
        {!! Form::radio('gender', '0' ) !!} Female
    @else
        {!! Form::radio('gender', '1') !!} Male
        {!! Form::radio('gender', '0', true) !!} Female
    @endif

</div>

{!! Form::submit('Save Cat', ['class' => 'btn btn-primary']) !!}
{!! Form::button('Back', ['class' => 'btn btn-default', 'onclick' => 'history.go(-1)']) !!}