@extends('partial.layouts.content_template')

@section('title')
    List cat
@endsection

@section('heading')
    Manage cats
@endsection

@section('operations')
    <p class="operations">
        <a href="{{route('admin.cats.create')}}" class="btn btn-primary">
            Create
        </a>

        <a class="operations__delete--hide hide btn btn-danger" onclick="document.getElementById('delete-selected-form').submit()">Delete selected
        </a>
    </p>

<form action="{{ route('admin.cats.destroy') }}" id="delete-selected-form" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <input type="hidden" name="ids" />
            </form>
@endsection

@section('panel_heading')
    List cat
@endsection

@section('main_content')

    @include('partial.flash.success')

    @include('admin.cats._filter')

    <br/>

    @if(count($cats) > 0)
        <table class="table table-striped table-bordered">
            <thead class="thead-default">
            <tr>
                <th><input type="checkbox" name="check-all"/></th>
                <th>#</th>
                <th>{{ sort_by('name', 'Cats', 'admin.cats.filter') }}</th>
                <th>Phone</th>
                <th>Breed</th>
                <th>Updated at</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>

            @foreach($cats as $key => $cat)
                <tr>
                    <td><input type="checkbox" name="check-selected" value="{{ $cat->id }}"/></td>
                    <td>{{ $cat->present()->startItem(request()->get('page')) + $key }}</td>
                    <td>
                        <a href={{ route('admin.cats.show', ['cat' => $cat]) }}>
                            {{ $cat->present()->name }}
                        </a>
                    </td>
                    <td>{{ $cat->present()->phone_number }}</td>
                    <td>{{ $cat->breed->name }}</td>
                    <td>{{ $cat->present()->updated_at }}</td>
                    <td>
                        <a href="{{route('admin.cats.edit', ['cat' => $cat])}}">
                            <button class="btn btn-info">Edit</button>
                        </a>
                        <button class="btn btn-danger"
                                onclick="document.getElementById('delete-cat-{{$cat->id}}').submit()">
                            Delete
                        </button>

                        <form id="delete-cat-{{$cat->id}}" style="display: none" method="POST"
                              action="{{route('admin.cats.delete', ['cat' => $cat])}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    @else
        @include('partial.table.empty')
    @endif

    <!-- Pagination -->
    {{ $cats->appends(request()->all())->links() }}

@stop