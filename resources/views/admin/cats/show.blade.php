@extends('partial.layouts.content_template')

@section('title')
    Show cat
@endsection

@section('heading')
    {{ $cat->name }}
@endsection


@section('panel_heading')
    Cat information
@endsection

@section('main_content')
    <div class="container">

        @include('partial.flash.success')

        <form id="deleteCat" action="{{ route('admin.cats.delete', ['cat' => $cat])  }}" name="deleteForm" method="POST"
              style="display: none;">
            {{csrf_field()}}
            {{method_field("DELETE")}}
        </form>

        <p>Last edited: {{ $cat->updated_at->diffForHumans() }}</p>

        <p>Date of Birth: {{ $cat->dob }}</p>
        <p>
            @if ($cat->breed)
                Breed:
                {{ link_to('cats/breeds/'.$cat->breed->name,
                $cat->breed->name) }}
            @endif
        </p>

        <a href="{{ route('admin.cats.edit', ['cat' => $cat]) }}">
            <span class="glyphicon glyphicon-edit"></span>
            Edit
        </a>

        <a href="#" onclick="confirmDeleteCat()">
            <span class="glyphicon glyphicon-trash"></span>
            Delete
        </a>

    </div>
@stop

@section('scripts')
    <script>
        function confirmDeleteCat() {
            event.preventDefault();
            var confirmed = confirm("Are you sure");
            if (confirmed) {
                document.getElementById('deleteCat').submit();
            }
        }
    </script>
@endsection