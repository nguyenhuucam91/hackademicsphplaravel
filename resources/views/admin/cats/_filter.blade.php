<!-- Filter cat -->
@php
    $breed = !array_key_exists('breed', $request) ? -1 : $request['breed'];
    $gender = !array_key_exists('gender', $request) ? -1 : $request['gender'];
    $keyword = !array_key_exists('keyword', $request) ? "" : $request['keyword'];
@endphp

<form class="form-inline" action="{{ route('admin.cats.filter') }}" method="get" id="filterCat">
    <div class="pull-left">
        <input type="hidden" value="{{csrf_token()}}"/>
        Filter by:
        <select class="form-control" name="breed" onchange="document.getElementById('filterCat').submit();">
            <option value="" selected>--All breed--</option>
            @foreach ($breeds as $key => $value)
                <option value="{{$key}}" @if($key == $breed) {{ 'selected' }} @endif>{{$value}}</option>
            @endforeach
        </select>

        <select class="form-control" name="gender" onchange="document.getElementById('filterCat').submit();">
            @if(strcmp($gender, '') == 0)
                <option value="" @if(strcmp($gender, '') == 0) {{ 'selected' }} @endif>-- All gender--</option>
                <option value="1"> Male</option>
                <option value="0"> Female</option>
            @else
                <option value="">-- All gender--</option>
                <option value="1" @if($gender == 1) {{ 'selected' }} @endif> Male</option>
                <option value="0" @if($gender == 0) {{ 'selected' }} @endif> Female</option>
            @endif

        </select>
    </div>

    <div class="pull-right">
        <div class="btn-group">
            <input id="searchinput" type="search" name='keyword' class="form-control"
                   placeholder="Enter search keyword ..." value="{{ $keyword }}">
            <span id="searchclear" class="glyphicon glyphicon-remove-circle"></span>
        </div>
    </div>

    <div class="clearfix"></div>
    <input name="sort" type="hidden" value="{{ request()->has('sort') ? request()->get('sort'): "" }}" />
</form>