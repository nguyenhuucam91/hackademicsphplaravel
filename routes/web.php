<?php

Auth::routes();


Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/password/recover/{token?}', 'Auth\ResetPasswordController@showResetPasswordForm')->name('reset.show-reset-form');
Route::post('/password/recover/{token}', 'Auth\ResetPasswordController@resetPassword')->name('reset.reset-password');

/**
 * Admin section
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    /**
     * Cat
     */
    Route::group(['prefix' => 'cats', 'as' => 'cats.'], function () {
        Route::get('/', 'CatsController@index')->name('index');
        Route::get('/create', 'CatsController@create')->name('create');
        Route::get('/filter', 'CatsController@filter')->name('filter');
        Route::post('/', 'CatsController@store')->name('store');
        Route::get('/{cat}/edit', 'CatsController@edit')->name('edit');
        Route::patch('/{cat}', 'CatsController@update')->name('update');
        Route::get('/{cat}', 'CatsController@show')->name('show');
        Route::delete('/destroy', 'CatsController@destroy')->name('destroy');
        Route::delete('/{cat}', 'CatsController@delete')->name('delete');
    });

    /**
     * Breed
     */

    Route::group(['prefix' => 'breeds', 'as' => 'breeds.'], function () {
        Route::get('/', 'BreedController@index')->name('index');
        Route::get('/create', 'BreedController@create')->name('create');
        Route::get('/filter', 'BreedController@filter')->name('filter');
        Route::post('/', 'BreedController@store')->name('store');
        Route::get('/{cat}/edit', 'BreedController@edit')->name('edit');
        Route::patch('/{cat}', 'BreedController@update')->name('update');
        Route::get('/{cat}', 'BreedController@show')->name('show');
        Route::delete('/destroy', 'BreedController@destroy')->name('destroy');
        Route::delete('/{cat}', 'BreedController@delete')->name('delete');
    });
});
